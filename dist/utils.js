"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
var Utils;
(function (Utils) {
    function deleteFolderRecursive(path) {
        var files = [];
        if (fs_1.existsSync(path)) {
            files = fs_1.readdirSync(path);
            files.forEach(function (file, index) {
                var curPath = path + "/" + file;
                if (fs_1.lstatSync(curPath).isDirectory()) {
                    deleteFolderRecursive(curPath);
                }
                else {
                    fs_1.unlinkSync(curPath);
                }
            });
            fs_1.rmdirSync(path);
        }
    }
    ;
    function clearDir(path) {
        if (fs_1.existsSync(path))
            deleteFolderRecursive(path);
        fs_1.mkdirSync(path);
    }
    Utils.clearDir = clearDir;
    function paths(tree) {
        var leaves = [];
        var walk = function (obj, path) {
            path = path ? path + "." : "";
            for (var n in obj) {
                if (obj.hasOwnProperty(n)) {
                    if (typeof obj[n] === "object" || obj[n] instanceof Array) {
                        walk(obj[n], path + n);
                    }
                    else {
                        leaves.push(path + n);
                    }
                }
            }
        };
        walk(tree, "");
        return leaves;
    }
    Utils.paths = paths;
})(Utils = exports.Utils || (exports.Utils = {}));
//# sourceMappingURL=utils.js.map