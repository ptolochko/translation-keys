#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var generator_1 = require("./generator");
var _a = process.argv.slice(2), input = _a[0], output = _a[1];
generator_1.Generator.generate(input, output);
//# sourceMappingURL=app.js.map