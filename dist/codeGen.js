"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("./utils");
var _ = require("lodash");
var CodeGen;
(function (CodeGen) {
    function constCode(name, value) {
        return "export const " + name + " = \"" + value + "\"";
    }
    function pathToName(path) {
        return _.camelCase(path);
    }
    function generate(json) {
        var paths = utils_1.Utils.paths(json);
        return "\nexport module Txt {\n  " + paths.map(function (x) { return constCode(pathToName(x), x); }).join("\n  ") + "\n}\n        ";
    }
    CodeGen.generate = generate;
})(CodeGen = exports.CodeGen || (exports.CodeGen = {}));
//# sourceMappingURL=codeGen.js.map