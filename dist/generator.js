"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var codeGen_1 = require("./codeGen");
var fs_1 = require("fs");
var jsyaml = require("js-yaml");
var Generator;
(function (Generator) {
    function getJson(path) {
        var x = fs_1.readFileSync(path + "/ru.yml");
        return jsyaml.safeLoad(x.toString());
    }
    Generator.getJson = getJson;
    function generate(input, output) {
        var json = getJson(input);
        var code = codeGen_1.CodeGen.generate(json);
        fs_1.writeFileSync(output, code);
    }
    Generator.generate = generate;
})(Generator = exports.Generator || (exports.Generator = {}));
//# sourceMappingURL=generator.js.map