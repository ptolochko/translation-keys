import {existsSync, lstatSync, mkdirSync, readdirSync, rmdirSync, unlinkSync} from "fs"
import * as _ from 'lodash'

export module Utils {

    function deleteFolderRecursive(path:string) {
        var files = [];
        if( existsSync(path) ) {
            files = readdirSync(path);
            files.forEach(function(file,index){
                var curPath = path + "/" + file;
                if(lstatSync(curPath).isDirectory()) { // recurse
                    deleteFolderRecursive(curPath);
                } else { // delete file
                    unlinkSync(curPath);
                }
            });
            rmdirSync(path);
        }
    };


    export function clearDir(path:string){
        if(existsSync(path)) deleteFolderRecursive(path)
        mkdirSync(path)
    }

    export function paths(tree:any) {
        var leaves:string[] = [];
        var walk = function(obj:any,path:string){
            path = path ? path + "." : "";
            for(var n in obj){
                if (obj.hasOwnProperty(n)) {
                    if(typeof obj[n] === "object" || obj[n] instanceof Array) {
                        walk(obj[n],path + n);
                    } else {
                        leaves.push(path + n);
                    }
                }
            }
        }
        walk(tree,"");
        return leaves;
    }


}