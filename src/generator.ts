import {CodeGen} from "./codeGen"
import {readFileSync, writeFileSync} from "fs"

import * as jsyaml from "js-yaml"

export module Generator {

    export function getJson(path:string){
        const x = readFileSync(path + "/ru.yml")
        return jsyaml.safeLoad(x.toString())
    }

    export function generate(input:string, output: string){
        const json = getJson(input)
        const code = CodeGen.generate(json)
        writeFileSync(output, code);
    }


}

