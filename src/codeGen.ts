import {Utils} from "./utils"
import * as _ from 'lodash'

export module CodeGen {

    function constCode(name:string, value:string){
        return `export const ${name} = "${value}"`
    }

    function pathToName(path:string){
        return _.camelCase(path)
    }

    export function generate(json:any){
        const paths = Utils.paths(json)
        return `
export module Txt {
  ${paths.map(x => constCode(pathToName(x), x)).join("\n  ")}
}
        `
    }
}