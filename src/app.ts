#!/usr/bin/env node
import {Generator} from "./generator"

const [input, output] = process.argv.slice(2);

Generator.generate(input,output)

